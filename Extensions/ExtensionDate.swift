import Foundation

extension Double {
    
    func dayStringFromTime(unixTime: Double) -> String {
        let dateFormatter = DateFormatter()
        let date = NSDate(timeIntervalSince1970: unixTime)
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: date as Date)
    }
    
    func hourStringFromTime(unixTime: Double) -> String {
        let dateFormatter = DateFormatter()
        let date = NSDate(timeIntervalSince1970: unixTime)
        dateFormatter.dateFormat = "HH"
        return dateFormatter.string(from: date as Date)
    }
    
    func hourMinStringFromTime(unixTime: Double) -> String {
        let dateFormatter = DateFormatter()
        let date = NSDate(timeIntervalSince1970: unixTime)
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date as Date)
    }
}


