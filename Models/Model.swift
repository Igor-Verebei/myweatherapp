
import Foundation

struct AllWeather: Decodable {
    var lat: Double?
    var lon: Double?
    var timezone: String?
    var current: Current?
    var hourly: [Hourly]?
    var daily: [Daily]?
}

struct Current: Decodable {
    var dt: Double?
    var sunrise: Double?
    var sunset: Double?
    var temp: Double?
    var pressure: Double?
    var humidity: Int?
    var weather: [Weather]?
}

struct Weather: Decodable {
    var icon: String?
    var main: String?
}

struct Hourly: Decodable {
    var dt: Double?
    var temp: Double?
    var weather: [Weather]?
}

struct Daily: Decodable {
    var dt: Double?
    var temp: Temp?
    var weather: [Weather]?
}

struct Temp: Decodable {
    var min: Double?
    var max: Double?
}

