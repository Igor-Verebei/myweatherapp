import Foundation

struct CityName: Decodable {
    var coord: Coordinates
    var name: String
}

struct Coordinates: Decodable {
    var lat: Double
    var lon: Double
}
