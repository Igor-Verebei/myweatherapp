import Foundation
import CoreLocation

class ApiManager: NSObject, CLLocationManagerDelegate{
    
    var lat: Double = 0
    var lon: Double = 0
    var wasCoordinated = false
    private let manager = CLLocationManager()
    static let shared = ApiManager()
    private override init(){}
    
    func getCoord(){
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        guard let location = locations.last else { return }
        let coordinates = location.coordinate
        let lat = coordinates.latitude
        let lon = coordinates.longitude
        print(coordinates)
        self.lat = lat
        self.lon = lon
        print("координаты \(self.lat), \(self.lon)")
        manager.stopUpdatingLocation()
    }
    
    func getWeather(complition: @escaping (AllWeather?)->()){
        let urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=\(self.lat)&lon=\(self.lon)&%20exclude=hourly,daily&appid=" + identifier
        guard let url = URL(string: urlString) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async {
                
                guard error == nil, let data = data else {return}
                
                do{
                    let allWeather = try JSONDecoder().decode(AllWeather.self, from: data)
                    complition(allWeather)
                    print()
                }catch let errorJson {
                    print(errorJson)
                }
            }
        }
        task.resume()
    }
}


