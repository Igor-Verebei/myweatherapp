
import UIKit
import Foundation
import Crashlytics

class SecondViewController: UIViewController {
    
    @IBOutlet weak var skyLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var currentDayLabel: UILabel!
    @IBOutlet weak var maxTempTedayLabel: UILabel!
    @IBOutlet weak var minTempTodayLabel: UILabel!
    @IBOutlet weak var locationOutlet: UIButton!
    private var allWeather = AllWeather()
    
    override func viewDidAppear(_ animated: Bool) {
        showWeather()
    }
    
    func showWeather() {
        ApiManager.shared.getCoord()
        ApiManager.shared.getWeather { [weak self] allWeather in
            guard let self = self else { return }
            guard let allWeather = allWeather else { return }
            self.allWeather = allWeather
            self.tableView.reloadData()
            self.setToday()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lineView.layer.cornerRadius = 3
        
        if !ApiManager.shared.wasCoordinated {
            ApiManager.shared.getCoord()
            ApiManager.shared.wasCoordinated = true
        }
    }
    
    @IBAction func locationButton(_ sender: UIButton) {
        showWeather()
        self.locationOutlet.isSelected = true
    }
    
    func setToday() {
        guard let currentName = allWeather.timezone else { return }
        let nameArray = currentName.components(separatedBy: "/")
        cityLabel.text = nameArray.last
        guard let currentTemp = allWeather.current?.temp else { return }
        tempLabel.text = String(Int(currentTemp) - 273) + "°"
        guard let currentDayText = allWeather.current?.dt else { return }
        currentDayLabel.text = currentDayText.dayStringFromTime(unixTime: currentDayText)
        guard let maxTemp = allWeather.daily?.first?.temp?.max else { return }
        maxTempTedayLabel.text = String(Int(maxTemp) - 273) + "°"
        guard let minTemp = allWeather.daily?.first?.temp?.min else { return }
        minTempTodayLabel.text = String(Int(minTemp) - 273) + "°"
        guard let sky = allWeather.current?.weather?.first?.main else { return }
        skyLabel.text = sky.localization()
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        guard let searchConroller = self.storyboard?.instantiateViewController(identifier: "SearchViewController") as? SearchViewController else { return }
        self.present(searchConroller, animated: true, completion: nil)
    }
}

extension SecondViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allWeather.daily?.count ?? 0 + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let firstCell = tableView.dequeueReusableCell(withIdentifier: "CollectionTableViewCell", for: indexPath) as? CollectionTableViewCell else { return UITableViewCell()}
            
            firstCell.setCV(weather: allWeather.hourly)
            return firstCell
            
        } else if indexPath.row == 7 {
            
            guard let thirdCell = tableView.dequeueReusableCell(withIdentifier: "SunRiseTableViewCell", for: indexPath) as? SunRiseTableViewCell else { return UITableViewCell()}
            thirdCell.setSunCell(sun: allWeather)
            return thirdCell
            
        } else {
            guard let secondCell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else { return UITableViewCell()}
            secondCell.setupTable(dailyWeather: allWeather.daily?[indexPath.row])
            return secondCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100
        } else {
            return 50
        }
    }
}
