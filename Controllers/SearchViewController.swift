import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let urlString = "https://api.openweathermap.org/data/2.5/weather?q=\(searchBar.text ?? "")&appid=" + identifier
        searchBar.resignFirstResponder()
        
        guard let url = URL(string: urlString) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard error == nil, let data = data else {return}
            
            do{
                let json = try JSONDecoder().decode(CityName.self, from: data)
                
                ApiManager.shared.lat = json.coord.lat
                ApiManager.shared.lon = json.coord.lon
                
                DispatchQueue.main.async {
                    guard let secondController = self.storyboard?.instantiateViewController(identifier: "SecondViewController") as? SecondViewController else {return}
                    self.present(secondController, animated: true, completion: nil)
                }
            }catch let errorJson {
                print(errorJson)
            }
        }
        task.resume()
    }
}
