import UIKit
import Kingfisher

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var minTempLabel: UILabel!
    
    func setupTable(dailyWeather: Daily?) {
        guard let day = dailyWeather?.dt else {return}
        dateLabel.text = day.dayStringFromTime(unixTime: day)
        guard let maxTemp = dailyWeather?.temp?.max else {return}
        maxTempLabel.text = String(Int(maxTemp) - 273) + "°"
        guard let minTemp = dailyWeather?.temp?.min else {return}
        minTempLabel.text = String(Int(minTemp) - 273) + "°"
        guard let dayIcon = dailyWeather?.weather?.first?.icon else {return}
        let iconString = "https://openweathermap.org/img/wn/" + dayIcon + "@2x.png"
        guard let url = URL(string: iconString) else {return}
        iconView.kf.setImage(with: url)
    }
}




