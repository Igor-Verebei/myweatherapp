import UIKit

class SunRiseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sunRiseLabel: UILabel!
    @IBOutlet weak var sunSetLabel: UILabel!
    
    func setSunCell(sun: AllWeather) {
        guard let sunRise = sun.current?.sunrise else { return }
        sunRiseLabel.text = sunRise.hourMinStringFromTime(unixTime: sunRise)
        guard let sunSet = sun.current?.sunset else { return }
        sunSetLabel.text = sunSet.hourMinStringFromTime(unixTime: sunSet)
        
    }
}
