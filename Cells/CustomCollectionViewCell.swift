import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var hhLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var hhTempLabel: UILabel!
    
    func setupCollection(hourlyWeather: Hourly?) {
        guard let hour = hourlyWeather?.dt else {return}
        hhLabel.text = hour.hourStringFromTime(unixTime: hour)
        guard let temp = hourlyWeather?.temp else {return}
        hhTempLabel.text = String(Int(temp) - 273) + "°"
        guard let dayIcon = hourlyWeather?.weather?.first?.icon else {return}
        let iconString = ("https://openweathermap.org/img/wn/" + dayIcon + "@2x.png")
        guard let url = URL(string: iconString) else {return}
        imageView.kf.setImage(with: url)
    }
}






